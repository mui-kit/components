import { emphasize } from '@material-ui/core/styles/colorManipulator'
import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing, palette }) => ({
  input: {
    // extra specificity needed for documentation
    '&.MuiInputBase-input': {
      display: 'flex',
      padding: 0,
      height: 'auto',
      minHeight: spacing(6)
    },
    display: 'flex',
    padding: 0,
    height: 'auto',
    minHeight: spacing(6)
  },
  noOptionsMessage: {
    padding: `${spacing(1)}px ${spacing(2)}px`
  },
  singleValue: {
    fontSize: 16,
    position: 'absolute',
    left: spacing(0.5),
    maxWidth: '100%'
  },
  placeholder: {
    position: 'absolute',
    left: spacing(0.5),
    fontSize: 16
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    marginTop: spacing(1),
    left: 0,
    right: 0
  },
  chip: {
    margin: `${spacing(1) / 2}px ${spacing(1) / 4}px`
  },
  chipFocused: {
    '&.MuiChip-root': {
      backgroundColor: emphasize(
        palette.type === 'light' ? palette.grey[300] : palette.grey[700],
        0.08
      )
    }
  }
}))
