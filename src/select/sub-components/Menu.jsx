import Paper from '@material-ui/core/Paper'
import { node, object, func } from 'prop-types'
import React from 'react'

const Menu = props => {
  const { selectProps, innerProps, getStyles, children } = props

  return (
    <Paper
      square
      className={selectProps.classes.paper}
      style={getStyles('menu', props)}
      {...innerProps}
    >
      {children}
    </Paper>
  )
}
Menu.propTypes = {
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Helper function that allows injecting styles */
  getStyles: func,
  children: node
}

export default Menu
