import IconButton from '@material-ui/core/IconButton'
import CancelIcon from '@material-ui/icons/Cancel'
import { object, func } from 'prop-types'
import React from 'react'

const ClearIndicator = props => {
  const { innerProps, getStyles } = props

  return (
    <IconButton style={getStyles('clearIndicator', props)} {...innerProps}>
      <CancelIcon />
    </IconButton>
  )
}

ClearIndicator.propTypes = {
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Helper function that allows injecting styles */
  getStyles: func
}

export default ClearIndicator
