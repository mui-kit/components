import React, { memo } from 'react'
import { isString } from '../../../utils'
import chunkText from './chunkText'

const ValueLabel = ({ label, search }) =>
  isString(label)
    ? chunkText(label, search).map(({ isTarget, text }, index) =>
        // eslint-disable-next-line react/no-array-index-key
        isTarget ? <b key={index}>{text}</b> : text
      )
    : label

export default memo(ValueLabel)
