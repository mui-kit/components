function chunkText(text, target) {
  if (!target) return [{ text }]
  const haystackLen = text.length
  const splits = []
  let start = 0
  let end = 0

  const lowerTarget = target.toLowerCase()
  const lowerText = text.toLowerCase()

  while (end < haystackLen) {
    const foundIdx = lowerText.indexOf(lowerTarget, end)
    end = ~foundIdx ? foundIdx : haystackLen

    if (end) {
      splits.push({ text: text.substring(start, end) })
      start = end
    }

    if (~foundIdx) {
      end += target.length
      splits.push({
        text: text.substring(start, end),
        isTarget: true
      })
      start = end
    }
  }

  return splits
}

export default chunkText
