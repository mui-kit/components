import MenuItem from '@material-ui/core/MenuItem'
import {
  bool,
  elementType,
  func,
  node,
  object,
  oneOfType,
  shape
} from 'prop-types'
import React from 'react'
import ValueLabel from './ValueLabel'

const Option = ({
  innerRef,
  isFocused,
  isSelected,
  innerProps,
  selectProps,
  isDisabled,
  children
}) => (
  <MenuItem
    disabled={isDisabled}
    buttonRef={innerRef}
    selected={isFocused}
    component="div"
    style={{
      fontWeight: isSelected ? 500 : 400,
      display: 'block'
    }}
    {...innerProps}
  >
    <ValueLabel label={children} search={selectProps.inputValue} />
  </MenuItem>
)
Option.propTypes = {
  /** Reference to the internal `react-select` element */
  innerRef: oneOfType([func, shape({ current: elementType })]),
  /** The focused state of the select */
  isFocused: bool,
  /** Whether the option is selected */
  isSelected: bool,
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Is the select disabled */
  isDisabled: bool,
  children: node
}

export default Option
