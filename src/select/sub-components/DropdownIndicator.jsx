import IconButton from '@material-ui/core/IconButton'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp'
import { bool, object, func } from 'prop-types'
import React from 'react'

const DropdownIndicator = props => {
  const { innerProps, selectProps, isDisabled, getStyles } = props

  return (
    <IconButton
      style={getStyles('dropdownIndicator', props)}
      disabled={isDisabled}
      {...innerProps}
    >
      {selectProps.menuIsOpen ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
    </IconButton>
  )
}

DropdownIndicator.propTypes = {
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Is the select disabled */
  isDisabled: bool,
  /** Helper function that allows injecting styles */
  getStyles: func
}

export default DropdownIndicator
