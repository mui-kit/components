import Chip from '@material-ui/core/Chip'
import CancelIcon from '@material-ui/icons/Cancel'
import { bool, func, node, object } from 'prop-types'
import React from 'react'

import { cl } from '../../utils'

const MultiValue = props => {
  const {
    selectProps,
    isFocused,
    removeProps,
    isDisabled,
    getStyles,
    children
  } = props

  return (
    <Chip
      tabIndex={-1}
      label={children}
      disabled={isDisabled}
      className={cl(
        selectProps.classes.chip,
        isFocused && selectProps.classes.chipFocused
      )}
      style={getStyles('multiValue', props)}
      onDelete={removeProps.onClick}
      deleteIcon={
        // eslint-disable-next-line react/jsx-wrap-multilines
        <CancelIcon
          style={getStyles('multiValueRemove', props)}
          {...removeProps}
        />
      }
      {...selectProps.chipProps}
    />
  )
}
MultiValue.propTypes = {
  /** The focused state of the select */
  isFocused: bool,
  /** Functions called on remove, see `react-select` */
  removeProps: object,
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Is the select disabled */
  isDisabled: bool,
  /** Helper function that allows injecting styles */
  getStyles: func,
  children: node
}

export default MultiValue
