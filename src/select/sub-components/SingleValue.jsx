import Typography from '@material-ui/core/Typography'
import { node, object, func } from 'prop-types'
import React from 'react'

const SingleValue = props => {
  const { selectProps, innerProps, getStyles, children } = props
  return (
    <Typography
      className={selectProps.classes.singleValue}
      style={getStyles('singleValue', props)}
      {...innerProps}
      noWrap
    >
      {children}
    </Typography>
  )
}
SingleValue.propTypes = {
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Helper function that allows injecting styles */
  getStyles: func,
  children: node
}

export default SingleValue
