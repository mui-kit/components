import TextField from '@material-ui/core/TextField'
import {
  bool,
  elementType,
  func,
  node,
  object,
  oneOfType,
  shape
} from 'prop-types'
import React from 'react'

// eslint-disable-next-line react/prop-types
const inputComponent = ({ inputRef, ...props }) => (
  <div ref={inputRef} {...props} />
)

const Control = ({
  selectProps,
  innerRef,
  innerProps,
  isDisabled,
  children
}) => (
  <TextField
    fullWidth
    disabled={isDisabled}
    label={selectProps.label}
    InputProps={{
      inputComponent,
      inputProps: {
        className: selectProps.classes.input,
        inputRef: innerRef,
        children,
        ...innerProps
      }
    }}
    {...selectProps.textFieldProps}
  />
)
Control.propTypes = {
  /** Reference to the internal `react-select` element */
  innerRef: oneOfType([func, shape({ current: elementType })]),
  /** Props passed to the inner element */
  innerProps: object,
  /** Props passed to the wrapping element for the group */
  selectProps: object,
  /** Is the select disabled */
  isDisabled: bool,
  children: node
}

export default Control
