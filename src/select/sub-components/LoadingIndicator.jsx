import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import React from 'react'
import { bool, func } from 'prop-types'

const LoadingIndicator = props => {
  const { isDisabled, getStyles } = props

  return (
    <IconButton style={getStyles('loadingIndicator', props)} disabled>
      <CircularProgress size={24} color={isDisabled ? 'inherit' : 'primary'} />
    </IconButton>
  )
}

LoadingIndicator.propTypes = {
  /** Is the select disabled */
  isDisabled: bool,
  /** Helper function that allows injecting styles */
  getStyles: func
}

export default LoadingIndicator
