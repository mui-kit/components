import Box from '@material-ui/core/Box'
import { node } from 'prop-types'
import React from 'react'

const ValueContainer = ({ children }) => (
  <Box
    display="flex"
    flexWrap="wrap"
    alignItems="center"
    flexGrow={1}
    overflow="hidden"
    position="relative"
  >
    {children}
  </Box>
)
ValueContainer.propTypes = {
  children: node
}

export default ValueContainer
