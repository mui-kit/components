import Typography from '@material-ui/core/Typography'
import { node, object, func } from 'prop-types'
import React from 'react'

const NoOptionsMessage = props => {
  const { selectProps, innerProps, getStyles, children } = props

  return (
    <Typography
      color="textSecondary"
      className={selectProps.classes.noOptionsMessage}
      style={getStyles('noOptionsMessage', props)}
      {...innerProps}
    >
      {children}
    </Typography>
  )
}
NoOptionsMessage.propTypes = {
  /** Props passed to the wrapping element for the group */
  innerProps: object,
  /** Props passed to the inner element */
  selectProps: object,
  /** Helper function that allows injecting styles */
  getStyles: func,
  children: node
}

export default NoOptionsMessage
