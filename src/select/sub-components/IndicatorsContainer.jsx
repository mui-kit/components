import Box from '@material-ui/core/Box'
import { node, func } from 'prop-types'
import React from 'react'

const IndicatorsContainer = props => {
  const { children, getStyles } = props

  return (
    <Box
      display="flex"
      flexWrap="wrap"
      alignContent="space-between"
      justifyContent="flex-end"
      flexShrink={9}
      style={getStyles('indicatorsContainer', props)}
    >
      {children}
    </Box>
  )
}
IndicatorsContainer.propTypes = {
  /** Helper function that allows injecting styles */
  getStyles: func,
  children: node
}

export default IndicatorsContainer
