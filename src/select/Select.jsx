import {
  arrayOf,
  bool,
  func,
  object,
  oneOfType,
  shape,
  string
} from 'prop-types'
import React, { forwardRef } from 'react'
import ReactSelect from 'react-select'

import useStyles from './Select.styles'
import {
  ClearIndicator,
  Control,
  DropdownIndicator,
  IndicatorsContainer,
  IndicatorSeparator,
  LoadingIndicator,
  LoadingMessage,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
} from './sub-components'

const styleOverrides = {
  clearIndicator: () => null,
  dropdownIndicator: () => null,
  indicatorsContainer: () => null,
  indicatorSeparator: () => null,
  loadingIndicator: () => null,
  loadingMessage: () => null,
  menu: () => null,
  multiValue: () => null,
  multiValueLabel: () => null,
  multiValueRemove: () => null,
  noOptionsMessage: () => null,
  placeholder: () => null,
  singleValue: () => null,
  valueContainer: () => null
}

const componentOverrides = {
  ClearIndicator,
  Control,
  DropdownIndicator,
  IndicatorsContainer,
  IndicatorSeparator,
  LoadingIndicator,
  LoadingMessage,
  Menu,
  MultiValue,
  NoOptionsMessage,
  Option,
  Placeholder,
  SingleValue,
  ValueContainer
}

/**
 * > Select component is based on **[react-select](https://github.com/jedwatson/react-select)** with a
 * **[material-ui](https://material-ui.com)** skin applied.
 *
 * Props are passed through, having no internal state of it's own, make sure to look over [react-select](https://github.com/jedwatson/react-select) for a full list of supported props, and capabilities.
 *
 * If you need a custom behavior / appearance, you can provide your own custom components through the **components** prop,
 * or custom styles, thus fully being able to control it's UI behavior.
 *
 * @visibleName Introduction
 */
const Select = forwardRef(
  (
    { Component = ReactSelect, components, textFieldProps, chipProps, ...rest },
    ref
  ) => {
    const classes = useStyles()
    const muiComponents = { ...componentOverrides, ...components }

    return (
      <Component
        ref={ref}
        textFieldProps={{
          ...textFieldProps,
          InputLabelProps: {
            shrink: true
          }
        }}
        chipProps={chipProps}
        components={muiComponents}
        styles={styleOverrides}
        {...{ ...rest, classes }}
      />
    )
  }
)

Select.defaultProps = {
  components: componentOverrides
}

Select.propTypes = {
  /** The components used to overlay [react-select](https://github.com/jedwatson/react-select), If you wish to overwrite a component, pass in an object with the appropriate namespace. */
  components: object,
  /** The default selected option(s) (uncontrolled) */
  defaultValue: object,
  /** The default select input value (uncontrolled) */
  defaultInputValue: string,
  /** Set to true to allow remove of selection with backspace or clicking on the x of the value(s) */
  isClearable: bool,
  /** The message displayed when no options are available */
  noOptionsMessage: func,
  /** The callback function called when the option is changed */
  onChange: func,
  /** The callback function called when the input is changed */
  onInputChange: func,
  /** The selectable options */
  options: arrayOf(
    shape({
      /** This value is searched over by [react-select](https://github.com/jedwatson/react-select) */
      value: string,
      /** The value displayed in the option dropdown */
      label: string
    })
  ),
  /** The props for Text Field container component */
  textFieldProps: object,
  /** The select input value (controlled) */
  inputValue: string,
  /** The value or values for the select (controlled) */
  value: oneOfType([object, arrayOf(object)]),
  /** The [react-select](https://github.com/jedwatson/react-select) component used
   * (creatable | async | async-creatable), which dictates the select behavior
   */
  Component: func
}

export default Select
