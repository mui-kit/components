### Example Usage

```jsx static
import React, { useState } from 'react'
import Select from '@mui-kit/components'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()

  handleSelectChange = selectedOption => {
    console.log(`Option selected:`, selectedOption)
    setSelected(selectedOption)
  }

  return (
    <Select value={selected} onChange={handleSelectChange} options={options} />
  )
}
```

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <Select
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Typography noWrap>
        Selected option: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with a pre-selected Value

You can set the beginging value through the **defaultValue** for an uncontrolled component, or **value** for a controlled one.
See [Uncontrolled Components](https://reactjs.org/docs/uncontrolled-components.html).

```jsx
import React, { useState } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState(options[2])

  return (
    <>
      <Select
        label="Controlled"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Box my={2}>
        <Typography noWrap>
          Selected option: <b>{(selected || {}).label}</b>
        </Typography>
      </Box>

      <Select
        defaultValue={options[0]}
        label="Uncontrolled"
        options={options}
      />
    </>
  )
}
;<Example />
```

### with disabled options

You can disable options dynamically by passing an **isOptionDisabled** function as a prop, that takes an option and
returns true when the option is disabled

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

const Example = () => {
  const [selected, setSelected] = useState()
  const [options, setOptions] = useState([
    { value: 'chocolate', label: 'Chocolate', disabled: true },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla', disabled: true },
    { value: 'caramel', label: 'Caramel' }
  ])

  const handleChange = idx => event => {
    const newOptions = [...options]
    newOptions[idx].disabled = event.target.checked

    setOptions(newOptions)
  }

  return (
    <>
      <Select
        label="With Disabled Options"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
        isOptionDisabled={option => option.disabled}
      />

      <FormGroup>
        {options.map(({ value, label, disabled }, idx) => (
          <FormControlLabel
            key={value}
            control={
              <Checkbox checked={disabled} onChange={handleChange(idx)} />
            }
            label={`${label} is disabled`}
          />
        ))}
      </FormGroup>

      <Typography noWrap>
        Selected option: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with option groups

You can partition your options in groups

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const optionGroups = [
  {
    label: 'Common',
    options: [
      { value: 'chocolate', label: 'Chocolate' },
      { value: 'vanilla', label: 'Vanilla' }
    ]
  },
  {
    label: 'Diverse',
    options: [
      { value: 'strawberry', label: 'Strawberry' },
      { value: 'caramel', label: 'Caramel' },
      { value: 'cookiesCream', label: 'Cookies and Cream' },
      { value: 'peppermint', label: 'Peppermint' }
    ]
  }
]

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <Select
        label="With Option Groups"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={optionGroups}
      />

      <Typography noWrap>
        Selected option: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

[mui-url]: https://material-ui.com
[sel-url]: https://github.com/jedwatson/react-select
