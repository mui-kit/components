When you type something not found in the list you are given the opportunity to create that option.

### Single

Support one selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <CreatableSelect
        label="Milkshake Flavour"
        placeholder="Select/Create..."
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### Multiple

Supports multiple selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <CreatableSelect
        isMulti
        label="Milkshake Flavour"
        placeholder="Select/Create..."
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Typography noWrap>
        Selected: <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### Tag input (Custom)

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const components = {
  DropdownIndicator: null
}

const createOption = label => ({
  label,
  value: label
})

const Example = () => {
  const [inputValue, setInputValue] = useState('')
  const [value, setValue] = useState([])

  const handleChange = (value, actionMeta) => {
    setValue(value)
  }

  const handleInputChange = inputValue => {
    setInputValue(inputValue)
  }

  const handleKeyDown = event => {
    if (!inputValue) return

    switch (event.key) {
      case 'Enter':
      case 'Tab':
        setInputValue('')
        setValue([...value, createOption(inputValue)])

        event.preventDefault()
    }
  }

  return (
    <>
      <CreatableSelect
        components={components}
        inputValue={inputValue}
        isClearable
        isMulti
        label="Create Tags"
        placeholder="Type something and press enter..."
        menuIsOpen={false}
        value={value}
        onChange={handleChange}
        onInputChange={handleInputChange}
        onKeyDown={handleKeyDown}
      />

      <Typography noWrap>
        Tags: <b>{(value || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```
