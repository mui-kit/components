import { arrayOf, bool, func, object, oneOfType, string } from 'prop-types'
import React, { forwardRef } from 'react'
import ReactAsyncSelect from 'react-select/async'
import Select from '../../Select'

/**
 * > AsyncSelect can be used to select from a list of async options.
 *
 * @visibleName Async
 */
const AsyncSelect = forwardRef((props, ref) => (
  <Select ref={ref} {...props} Component={ReactAsyncSelect} />
))
AsyncSelect.propTypes = {
  /** The default set of options to show before the user starts searching. When set to true, the results for loadOptions('') will be auto loaded. */
  defaultOptions: oneOfType([bool, object]),
  /** The default select input value (uncontrolled) */
  defaultInputValue: string,
  /** If cacheOptions is truthy, then the loaded data will be cached. The cache will remain until cacheOptions changes value */
  cacheOptions: bool,
  /** Set to true to allow remove of selection with backspace or clicking on the x of the value(s) */
  isClearable: bool,
  /** Will cause the select to be displayed in the loading state, even if the Async select is not currently waiting for loadOptions to resolve */
  isLoading: bool,
  /** The callback function called when the option is changed */
  onChange: func,
  /** The callback function called when the input is changed */
  onInputChange: func,
  /** Function that returns a promise, which is the set of options to be used once the promise resolves */
  loadOptions: func,
  /** The props for Text Field container component */
  textFieldProps: object,
  /** The select input value (controlled) */
  inputValue: string,
  /** The value or values for the select (controlled) */
  value: oneOfType([object, arrayOf(object)]),
  /** Close the select menu when the user selects an option */
  closeMenuOnSelect: bool
}

export default AsyncSelect
