### Single

Support one selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncSelect
        label="Milkshake Flavour"
        placeholder="Select your favorite(s)"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### Multiple

Supports multiple selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncSelect
        label="Milkshake Flavour(s)"
        placeholder="Select your favorite(s)"
        isMulti
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with Default options

The defaultOptions prop determines "when" your remote request is initially fired.
There are two valid values for this property. Providing an option array to this prop will populate the initial set of options
used when opening the select, at which point the remote load only occurs when filtering the options (typing in the control).
Providing the prop by itself (or with 'true') tells the control to immediately fire the remote request,
described by your loadOptions, to get those initial values for the Select.

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncSelect
        defaultOptions={options.slice(0, 3)}
        label="Milkshake Flavour"
        placeholder="Select your favorite(s)"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with Caching

If cacheOptions is truthy, then the loaded data will be cached. The cache will remain until cacheOptions changes value

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncSelect
        label="Milkshake Flavour"
        placeholder="Select your favorite(s)"
        cacheOptions
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```
