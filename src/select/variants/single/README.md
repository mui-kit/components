```jsx
import React, { useRef, useState } from 'react'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()
  const [state, setState] = React.useState({
    isClearable: true,
    isSearchable: true,
    isDisabled: false,
    isLoading: false
  })
  const selectEl = useRef()

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked })
  }

  const focusSelect = () => {
    selectEl.current.focus()
  }

  return (
    <>
      <SingleSelect
        ref={selectEl}
        label="Milkshake Flavour"
        placeholder="Select your favorite(s)"
        isClearable={state.isClearable}
        isSearchable={state.isSearchable}
        isDisabled={state.isDisabled}
        isLoading={state.isLoading}
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Box my={2}>
        <Button variant="outlined" onClick={() => selectEl.current.focus()}>
          Focus Select
        </Button>
      </Box>

      <FormGroup>
        <FormControlLabel
          control={
            <Checkbox
              checked={state.isClearable}
              onChange={handleChange('isClearable')}
            />
          }
          label="Clearable"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isSearchable}
              onChange={handleChange('isSearchable')}
            />
          }
          label="Searchable"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isDisabled}
              onChange={handleChange('isDisabled')}
            />
          }
          label="Disabled"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isLoading}
              onChange={handleChange('isLoading')}
            />
          }
          label="Loading"
        />
      </FormGroup>

      <Typography noWrap>
        Selected option: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```
