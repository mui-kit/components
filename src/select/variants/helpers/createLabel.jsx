import React from 'react'

const createLabel = (createText = 'Create...') => input => (
  <>
    {createText}
    {'"'}
    <b>{input}</b>
    {'"'}
  </>
)

export default createLabel
