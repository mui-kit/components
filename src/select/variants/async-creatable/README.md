### Single

Supports one selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncCreatableSelect
        defaultOptions={options.slice(0, 3)}
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### Multiple

Supports multiple selected options

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncCreatableSelect
        isMulti
        defaultOptions={options.slice(0, 3)}
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### Allow Creating while loading

Allow options to be created while the isLoading prop is true. Useful to prevent the "create new ..." option being displayed while async results are still being loaded.

```jsx
import React, { useState } from 'react'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const filterOptions = searchText =>
  options.filter(
    opt =>
      searchText && opt.label.toLowerCase().includes(searchText.toLowerCase())
  )

const promiseOptions = searchText =>
  new Promise(resolve => {
    setTimeout(() => {
      resolve(filterOptions(searchText))
    }, 1000)
  })

const Example = () => {
  const [selected, setSelected] = useState()

  return (
    <>
      <AsyncCreatableSelect
        defaultOptions={options.slice(0, 3)}
        allowCreateWhileLoading
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        loadOptions={promiseOptions}
      />

      <Typography noWrap>
        Selected: <b>{(selected || {}).label}</b>
      </Typography>
    </>
  )
}
;<Example />
```
