```jsx
import React, { useRef, useState } from 'react'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState()
  const [state, setState] = React.useState({
    isClearable: true,
    isSearchable: true,
    isDisabled: false,
    isLoading: false,
    closeMenuOnSelect: true
  })
  const selectEl = useRef()

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked })
  }

  const focusSelect = () => {
    selectEl.current.focus()
  }

  return (
    <>
      <MultipleSelect
        ref={selectEl}
        label="Milkshake Flavour(s)"
        placeholder="Select your favorite(s)"
        isClearable={state.isClearable}
        isSearchable={state.isSearchable}
        isDisabled={state.isDisabled}
        isLoading={state.isLoading}
        closeMenuOnSelect={state.closeMenuOnSelect}
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Box my={2}>
        <Button variant="outlined" onClick={() => selectEl.current.focus()}>
          Focus Select
        </Button>
      </Box>

      <FormGroup>
        <FormControlLabel
          control={
            <Checkbox
              checked={state.isClearable}
              onChange={handleChange('isClearable')}
            />
          }
          label="Clearable"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isSearchable}
              onChange={handleChange('isSearchable')}
            />
          }
          label="Searchable"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isDisabled}
              onChange={handleChange('isDisabled')}
            />
          }
          label="Disabled"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.isLoading}
              onChange={handleChange('isLoading')}
            />
          }
          label="Loading"
        />

        <FormControlLabel
          control={
            <Checkbox
              checked={state.closeMenuOnSelect}
              onChange={handleChange('closeMenuOnSelect')}
            />
          }
          label="Close Menu on Select"
        />
      </FormGroup>

      <Typography noWrap>
        Selected option(s):
        <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with pre-selected Values

```jsx
import React, { useState } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const Example = () => {
  const [selected, setSelected] = useState(options.slice(0, 2))

  return (
    <>
      <MultipleSelect
        label="Preselected"
        value={selected}
        onChange={selectedOption => {
          setSelected(selectedOption)
        }}
        options={options}
      />

      <Typography noWrap>
        Selected option(s):
        <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```

### with fixed Values (Custom)

```jsx
import React, { useState } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

const options = [
  { value: 'chocolate', label: 'Chocolate', isFixed: true },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla', isFixed: true },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const styles = {
  clearIndicator: () => null,
  multiValue: (base, state) =>
    state.data.isFixed && {
      backgroundColor: 'gray',
      fontWeight: 'bold',
      color: 'white'
    },
  multiValueRemove: (base, state) => state.data.isFixed && { display: 'none' }
}

const orderOptions = values => {
  return values.filter(v => v.isFixed).concat(values.filter(v => !v.isFixed))
}

const Example = () => {
  const [selected, setSelected] = useState(orderOptions(options.slice(0, 3)))

  const onChange = (value, { action, removedValue }) => {
    switch (action) {
      case 'remove-value':
      case 'pop-value':
        if (removedValue.isFixed) {
          return
        }
        break
      case 'clear':
        value = options.filter(v => v.isFixed)
        break
    }

    value = orderOptions(value)
    setSelected(value)
  }

  return (
    <>
      <MultipleSelect
        label="With Fixed Values"
        styles={styles}
        value={selected}
        onChange={onChange}
        isClearable={options.some(v => !v.isFixed)}
        options={options}
      />

      <Typography noWrap>
        Selected option(s):
        <b>{(selected || []).map(({ label }) => label).join(', ')}</b>
      </Typography>
    </>
  )
}
;<Example />
```
