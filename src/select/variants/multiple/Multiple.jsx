import {
  arrayOf,
  bool,
  func,
  object,
  oneOfType,
  shape,
  string
} from 'prop-types'
import React, { forwardRef } from 'react'
import Select from '../../Select'

/**
 * > MultipleSelect can be used to select multiple options from a list of available options.
 *
 * @visibleName Multiple
 */
const MultipleSelect = forwardRef((props, ref) => (
  <Select ref={ref} {...props} isMulti />
))
MultipleSelect.propTypes = {
  /** The default selected option(s) (uncontrolled) */
  defaultValue: object,
  /** The default select input value (uncontrolled) */
  defaultInputValue: string,
  /** Set to true to allow remove of selection with backspace or clicking on the x of the value(s) */
  isClearable: bool,
  /** The callback function called when the option is changed */
  onChange: func,
  /** The callback function called when the input is changed */
  onInputChange: func,
  /** The selectable options */
  options: arrayOf(
    shape({
      /** This value is searched over by [react-select](https://github.com/jedwatson/react-select) */
      value: string,
      /** The value displayed in the option dropdown */
      label: string
    })
  ),
  /** The props for Text Field container component */
  textFieldProps: object,
  /** The select input value (controlled) */
  inputValue: string,
  /** The value or values for the select (controlled) */
  value: oneOfType([object, arrayOf(object)]),
  /** Close the select menu when the user selects an option */
  closeMenuOnSelect: bool
}

export default MultipleSelect
