const isString = obj =>
  Object.prototype.toString.call(obj) === '[object String]'

export default isString
