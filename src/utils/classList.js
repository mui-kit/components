const cl = (...classes) => classes.filter(item => !!item).join(' ')

export default cl
