import React from 'react'
import { hot } from 'react-hot-loader/root'
import Root from './components/layout'
import SelectDemo from '@components/routes/select-demo'

const App = () => (
	<Root>
		<SelectDemo />
	</Root>
)

export default hot(App)
