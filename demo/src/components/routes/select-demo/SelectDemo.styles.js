import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing }) => ({
  header: {
    marginBottom: spacing(2)
  }
}))
