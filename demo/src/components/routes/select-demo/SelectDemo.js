import Typography from '@material-ui/core/Typography'
import React from 'react'
import Content from './content'
import EditSave from './edit-save'
import { SelectProvider } from './SelectContext'
import useStyles from './SelectDemo.styles'

const SelectDemo = () => {
  const classes = useStyles()

  return (
    <SelectProvider>
      <Typography variant="h1" className={classes.header}>
        Select Demo
      </Typography>
      <EditSave />

      <Content />
    </SelectProvider>
  )
}

export default SelectDemo
