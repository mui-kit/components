import { localStorageKeys } from '@components/layout/properties.json'
import { getLocalStorageItem, setLocalStorageItem } from '@utils'
import React, { createContext, useContext, useReducer } from 'react'

const SelectStateContext = createContext()
const SelectDispatchContext = createContext()

function selectReducer(state, action) {
  switch (action.type) {
    case selectTypes.isEdit:
      return {
        ...state,
        draftSkills: [...state.skills],
        isEdit: true
      }
    case selectTypes.isSave:
      setLocalStorageItem(localStorageKeys.skills, state.draftSkills)
      return {
        ...state,
        skills: [...state.draftSkills],
        isEdit: false
      }
    case selectTypes.cancelSave:
      return {
        ...state,
        isEdit: false
      }
    case selectTypes.updateSelect:
      return {
        ...state,
        draftSkills: action.update
      }

    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

const selectTypes = {
  isEdit: 'isEdit',
  isSave: 'isSave',
  cancelSave: 'cancelSave',
  updateSelect: 'updateSelect'
}
const initialState = {
  skills: getLocalStorageItem(localStorageKeys.skills, [
    { label: 'javascript', value: 'javascript' },
    { label: 'reactjs', value: 'reactjs' }
  ]),
  draftSkills: []
}

const SelectProvider = ({ children }) => {
  const [state, dispatch] = useReducer(selectReducer, initialState)

  return (
    <SelectStateContext.Provider value={state}>
      <SelectDispatchContext.Provider value={dispatch}>
        {children}
      </SelectDispatchContext.Provider>
    </SelectStateContext.Provider>
  )
}

const useSelectState = () => {
  const context = useContext(SelectStateContext)

  if (context === undefined) {
    throw new Error('useSelectState must be used within a SelectProvider')
  }
  return context
}

const useSelectDispatch = () => {
  const context = useContext(SelectDispatchContext)

  if (context === undefined) {
    throw new Error('useSelectDispatch must be used within a SelectProvider')
  }
  return context
}

export { SelectProvider, selectTypes, useSelectState, useSelectDispatch }
