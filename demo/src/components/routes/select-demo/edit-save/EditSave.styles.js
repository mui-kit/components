import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing }) => ({
	buttonMargin: {
		marginRight: spacing()
	}
}))
