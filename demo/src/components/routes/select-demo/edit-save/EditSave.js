import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Cancel from '@material-ui/icons/Cancel'
import Edit from '@material-ui/icons/Edit'
import Save from '@material-ui/icons/Save'
import React from 'react'
import {
  selectTypes,
  useSelectDispatch,
  useSelectState
} from '../SelectContext'
import useStyles from './EditSave.styles'

const EditSave = () => {
  const classes = useStyles()
  const { isEdit } = useSelectState()
  const dispatch = useSelectDispatch()

  return (
    <Box textAlign="right" mb={1}>
      {isEdit ? (
        <>
          <Button
            startIcon={<Cancel />}
            className={classes.buttonMargin}
            onClick={() => dispatch({ type: selectTypes.cancelSave })}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            startIcon={<Save />}
            onClick={() => dispatch({ type: selectTypes.isSave })}
          >
            Save
          </Button>
        </>
      ) : (
        <Button
          variant="contained"
          startIcon={<Edit />}
          onClick={() => dispatch({ type: selectTypes.isEdit })}
        >
          Edit
        </Button>
      )}
    </Box>
  )
}

export default EditSave
