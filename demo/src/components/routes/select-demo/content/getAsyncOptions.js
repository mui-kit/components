import debounce from 'debounce-promise'

const API_KEY = '2PQD4Wx)J3hCBsPvXTJDKg(('
const getAsyncOptions = async query => {
  const response = await fetch(
    `https://api.stackexchange.com/2.2/tags?key=${API_KEY}&site=stackoverflow&order=desc&sort=popular&inname=${query}&filter=default`
  )
  const asJson = await response.json()

  return asJson.items.map(({ name }) => ({ label: name, value: name }))
}

export default debounce(inputValue => getAsyncOptions(inputValue), 350, {
  leading: true
})
