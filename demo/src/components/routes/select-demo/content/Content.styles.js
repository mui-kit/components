import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing }) => ({
	chipMargin: {
		marginRight: spacing(0.5)
	},
	sectionTitle: {
		marginBottom: spacing(0.5)
	}
}))
