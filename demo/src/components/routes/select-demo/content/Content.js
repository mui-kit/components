import Chip from '@material-ui/core/Chip'
import Typography from '@material-ui/core/Typography'
import React from 'react'

import EditTags from '../../../edit-tags/EditTags'
import { useSelectDispatch, useSelectState } from '../SelectContext'
import useStyles from './Content.styles'

const Content = () => {
  const { isEdit, skills, draftSkills } = useSelectState()
  const dispatch = useSelectDispatch()
  const classes = useStyles()

  return (
    <>
      <Typography variant="h5" className={classes.sectionTitle}>
        Skills
      </Typography>

      {isEdit ? (
        <>
          <EditTags />
          {/* <Select
            isMulti
            cacheOptions
            defaultOptions
            createOptionPosition="first"
            loadOptions={inputValue => getAsyncOptions(inputValue)}
            value={draftSkills}
            onChange={value =>
              dispatch({
                type: selectTypes.updateSelect,
                update: value
              })
            }
            closeMenuOnSelect={false}
            chipProps={{
              variant: 'outlined',
              color: 'primary'
            }}
          /> */}
        </>
      ) : (
        skills.map(({ label, value }) => (
          <Chip key={value} label={label} className={classes.chipMargin} />
        ))
      )}
    </>
  )
}

export default Content
