import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing }) => ({
	content: {
		marginTop: spacing(4)
	}
}))
