import Container from '@material-ui/core/Container'
import React from 'react'
import useStyles from './Content.styles'

function Content({ children }) {
	const classes = useStyles()

	return (
		<Container component="main" className={classes.content}>
			{children}
		</Container>
	)
}

export { Content }
