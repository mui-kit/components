import { Content } from '@components/layout'
import CssBaseline from '@material-ui/core/CssBaseline'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import React from 'react'

const Root = ({ children }) => (
  <ThemeProvider theme={createMuiTheme()}>
    <CssBaseline />

    <Content>{children}</Content>
  </ThemeProvider>
)

export default Root
