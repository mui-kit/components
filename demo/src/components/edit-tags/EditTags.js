import { Button, Chip, Popover } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import { Select } from '@mui-kit/components'
import React, { useState } from 'react'

import { useSelectState } from '../routes/select-demo/SelectContext'
import useStyles from './EditTags.styles'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
  { value: 'caramel', label: 'Caramel' },
  { value: 'cookiesCream', label: 'Cookies and Cream' },
  { value: 'peppermint', label: 'Peppermint' }
]

const EditTags = () => {
  const [isPopupOpen, setIsPopupOpen] = useState(false)
  const [anchorEl, setAnchorEl] = useState(null)
  const [selected, setSelected] = useState()

  const { skills } = useSelectState()
  const classes = useStyles()

  const handleDelete = () => {}

  const handlePopupOpen = evt => {
    setAnchorEl(evt.currentTarget)
    setIsPopupOpen(true)
  }

  const handlePopupClose = () => {
    setAnchorEl(null)
    setIsPopupOpen(false)
  }

  const handleSelectChange = selectedOption => {
    console.log(`Option selected:`, selectedOption)
    setSelected(selectedOption)
    setIsPopupOpen(false)
  }

  return (
    <>
      {skills.map(({ label, value }) => (
        <Chip
          variant="outlined"
          color="primary"
          key={value}
          label={label}
          onDelete={handleDelete}
          className={classes.elSpacing}
        />
      ))}

      <Button
        variant="contained"
        color="primary"
        startIcon={<AddIcon />}
        onClick={handlePopupOpen}
      >
        Add Tag
      </Button>

      <Popover
        open={isPopupOpen}
        anchorEl={anchorEl}
        onClose={handlePopupClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        PaperProps={{
          className: classes.selectWrapper
        }}
      >
        <Select
          autoFocus
          menuIsOpen
          value={selected}
          onChange={handleSelectChange}
          options={options}
        />
      </Popover>
    </>
  )
}

export default EditTags
