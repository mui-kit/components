import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ spacing }) => ({
  elSpacing: {
    marginRight: spacing(0.5)
  },
  selectWrapper: {
    marginTop: spacing(),
    overflow: 'visible',
    minWidth: '15rem'
  }
}))
