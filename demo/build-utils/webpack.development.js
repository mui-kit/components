const webpack = require('webpack')

module.exports = () => ({
  devtool: 'source-map',
  devServer: {
    hot: true,
    open: true,
    overlay: true,
    noInfo: true
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: { cacheDirectory: true }
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      }
    ]
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
  output: {
    filename: '[name].js'
  }
})
