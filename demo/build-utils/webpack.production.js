const CompressionPlugin = require('compression-webpack-plugin')
const OfflinePlugin = require('offline-plugin')

module.exports = () => ({
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.html$/,
				loader: 'html-loader',
				options: {
					minimize: true
				}
			}
		]
	},
	plugins: [
		new OfflinePlugin({
			publicPath: '/components/',
			autoUpdate: true
		}),
		new CompressionPlugin({
			test: /\.(html|css|js)(\?.*)?$/i // only compressed html/css/js, skips compressing sourcemaps etc
		})
	],
	output: {
		publicPath: '/components/',
		filename: '[name].[contenthash:8].js'
	}
})
