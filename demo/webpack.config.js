const path = require('path')

const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const webpackMerge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const modeConfiguration = env => require(`./build-utils/webpack.${env}`)(env)

const mapToFolder = (dependencies, folder) =>
	dependencies.reduce(
		(acc, dependency) => ({
			[dependency]: path.resolve(`${folder}/${dependency}`),
			...acc
		}),
		{}
	)

module.exports = (env, { mode = 'production' }) =>
	webpackMerge(
		{
			output: {
				path: path.resolve(__dirname, '../build')
			},
			optimization: {
				splitChunks: {
					chunks: 'all',
					cacheGroups: {
						react: {
							test: /[\\/]node_modules[\\/]((react).*)[\\/]/,
							name: 'react'
						},
						commons: {
							test: /[\\/]node_modules[\\/]((?!react).*)[\\/]/,
							name: 'common'
						}
					}
				}
			},
			plugins: [
				new CleanWebpackPlugin(),
				new HtmlWebpackPlugin({
					favicon: './public/favicon.ico',
					template: './public/index.html',
					filename: './index.html'
				})
			],
			resolve: {
				alias: {
					'@components': path.resolve(__dirname, 'src/components/'),
					'@utils': path.resolve(__dirname, 'src/utils/'),
					'react-dom': '@hot-loader/react-dom',
					...mapToFolder(
						[
							'@material-ui/core',
							'@material-ui/icons',
							'@material-ui/styles',
							'react',
							'react-dom',
							'prop-types'
						],
						'./node_modules'
					)
				},
				extensions: ['.js', '.jsx']
			}
		},
		modeConfiguration(mode)
	)
