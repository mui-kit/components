const CompressionPlugin = require('compression-webpack-plugin')
const path = require('path')

module.exports = {
  title: 'Components',
  ignore: ['**/*.styles.js', '**/index.js'],
  components: 'src/**/[A-Z]*.jsx?$',
  skipComponentsWithoutExample: true,
  webpackConfig(env) {
    const isDev = env === 'development'

    return {
      module: {
        rules: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/env',
                  {
                    targets: [
                      'last 1 chrome version',
                      'last 1 firefox version',
                      'last 1 safari version'
                    ]
                  }
                ],
                '@babel/react'
              ],
              ...(isDev && { cacheDirectory: true })
            }
          },
          {
            test: /\.css$/,
            use: ['style-loader', 'css-loader']
          }
        ]
      },
      ...(!isDev && {
        plugins: [
          new CompressionPlugin({
            test: /\.(html|css|js)(\?.*)?$/i // only compressed html/css/js, skips compressing sourcemaps etc
          })
        ]
      })
    }
  },
  require: ['./docs/index.css'],
  styleguideDir: './build/docs',
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'docs/Wrapper')
  },
  sections: [
    {
      name: 'Welcome',
      content: 'docs/Welcome.md'
    },
    {
      name: 'Select',
      components: [
        'src/select/Select.jsx',
        'src/select/variants/single/Single.jsx',
        'src/select/variants/multiple/Multiple.jsx',
        'src/select/variants/creatable/Creatable.jsx',
        'src/select/variants/async/Async.jsx',
        'src/select/variants/async-creatable/AsyncCreatable.jsx'
      ]
    }
  ]
}
