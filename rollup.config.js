import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import pkg from './package.json'

const externalDeps = ['react', 'prop-types', '@material-ui']
const external = uri => externalDeps.some(dep => uri.startsWith(dep))

const babelConfig = targets => ({
  include: ['src/**/*'],
  exclude: 'node_modules/**', // only transpile our source code
  babelrc: false,
  presets: [
    [
      '@babel/env',
      {
        modules: false,
        targets
      }
    ],
    '@babel/react'
  ]
})

export default [
  {
    input: 'src/index.js',
    external,
    output: {
      file: pkg.main,
      format: 'cjs',
      exports: 'named'
    },
    plugins: [
      resolve({
        extensions: ['.js', '.jsx']
      }),
      babel(babelConfig({ node: 'current' }))
    ]
  },
  {
    input: 'src/index.js',
    external,
    output: {
      file: pkg.module,
      format: 'es',
      exports: 'named'
    },
    plugins: [
      resolve({
        extensions: ['.js', '.jsx']
      }),
      babel(babelConfig({ esmodules: true }))
    ]
  }
]
