import CssBaseline from '@material-ui/core/CssBaseline'
import { createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import { node } from 'prop-types'
import React from 'react'

const theme = createMuiTheme({})

const Wrapper = ({ children }) => (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {children}
  </ThemeProvider>
)

Wrapper.propTypes = {
  children: node
}

export default Wrapper
