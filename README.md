# [Components][demo-url]

> Shared [Material-UI](https://material-ui.com) based Component Library for React Apps

[![npm][npm]][npm-url]
[![downloads][downloads]][downloads-url]

**[Demo][demo-url]** | **[Documentation][docu-url]** | **[Issues][issues-url]**

## Install

Make sure you have the latest version of [node](https://nodejs.org/) and [npm](https://www.npmjs.com) installed.

```bash
npm install --save @mui-kit/components
```

`Components` requires [react][react-url], [react-dom][react-dom-url], [@material-ui/core][mui-core-url] and [@material-ui/icons][mui-icons-url] as peer dependencies.

```bash
npm install --save react react-dom @material-ui/core @material-ui/icons
```

## Usage

```jsx
import React from 'react'
import { Select } from '@mui-kit/components'

const options = [
  { value: 'chocolate', label: 'Chocolate' },
  { value: 'strawberry', label: 'Strawberry' },
  { value: 'vanilla', label: 'Vanilla' },
]

const Example = () => <Select options={options} />
```

## Contributing

[There are plenty of opportunities to get involved](https://gitlab.com/mui-kit/components/issues). Pick an outstanding task, let us know what you are working on and fire away with any questions.

The package is made up of 2 main folders:

- /src contains the React components
- /demo is our demo website

To setup and run a local copy:

1.  Clone this repo with `git clone https://gitlab.com/mui-kit/components.git`
2.  Run `npm install` in the root folder
3.  Run `npm install` in the demo folder
4.  In separate terminal windows, run `npm start` in the root and demo folders.

You should now be up and running with live browser reloading of the demo website while you work on the Components in the `/src` folder.

When you're done working on your changes, submit a PR with the details and include a screenshot if you've changed anything visually.

[npm]: https://img.shields.io/npm/v/@mui-kit/components.svg
[npm-url]: https://npmjs.com/package/@mui-kit/components
[downloads]: https://img.shields.io/npm/dt/@mui-kit/components.svg
[downloads-url]: https://npmjs.com/package/@mui-kit/components
[demo-url]: https://mui-kit.gitlab.io/components
[docu-url]: https://mui-kit.gitlab.io/components/docs
[issues-url]: https://gitlab.com/mui-kit/components/issues
[react-url]: https://www.npmjs.com/package/react
[react-dom-url]: https://www.npmjs.com/package/react-dom
[mui-core-url]: https://www.npmjs.com/package/@material-ui/core
[mui-icons-url]: https://www.npmjs.com/package/@material-ui/icons
